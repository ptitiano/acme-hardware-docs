# ACME Hardware Documents

## Revision B Documents
  * [ACME CAPE Schematics (pdf)](cape/rev_B/Design/ACME_CAPE_RevB_Schematics.pdf)
  * [POWER PROBE JACK Schematics (pdf)](power_probe_jack/rev_B/Design/POWER_PROBE_JACK_RevB_Schematics.pdf)
  * [POWER PROBE HE10 Schematics (pdf)](power_probe_he10/rev_B/Design/POWER_PROBE_HE10_RevB_Schematics.pdf)
  * [POWER PROBE USB Schematics (pdf)](power_probe_usb/rev_B/Design/POWER_PROBE_USB_RevB_Schematics.pdf)
